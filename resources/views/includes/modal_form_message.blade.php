
<div id="modal_form_message" class="modal modal-success-confirmation">
    <div class="modal-content">
        <h5 class="blue-text text-darken-4">Operation Successful<i class="material-icons right ">info</i></h5>
        <p>{{$formMessage}}</p>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">OK</a>
    </div>
</div>

