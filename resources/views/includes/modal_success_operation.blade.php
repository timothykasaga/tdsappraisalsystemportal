
<div id="modal_successful_operation" class="modal modal-success-confirmation">
    <div class="modal-content">
        <h5>Operation Successful</h5>
        <p>{{$successfulOperationMessage}}</p>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">OK</a>
    </div>
</div>
