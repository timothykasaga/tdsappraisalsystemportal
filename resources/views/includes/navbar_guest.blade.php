
<nav>
    <div class="nav-wrapper blue darken-4">

        <a href="#" class="brand-logo">{{config('app.name')}}</a>
        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>

        <ul class="right hide-on-med-and-down ">

            @if(session('user'))
                <li>
                    <a href="{{route('singout_admin')}}" >
                        <i class="material-icons left">lock_open</i>Logout
                    </a>
                </li>
            @endif


        </ul>


        <ul class="side-nav" id="mobile-demo">

            @if(session('user'))

                <li>
                    <a href="{{route('singout_admin')}}" >
                        <i class="material-icons left">lock_open</i>Logout
                    </a>
                </li>

            @endif


        </ul>

    </div>
</nav>

<ul id="drop_down_change_password" class="dropdown-content">
    <li><a href="{{route('users.change-password-form')}}">Change Password</a></li>
</ul>
 