<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ URL::asset('css/materialize.min.css') }}" type="text/css"/>
    @yield('page-level-css')
    {{--<link rel="stylesheet" href="{{ URL::asset('css/styles-min.css') }}" type="text/css"/>--}}
    <link rel="stylesheet" href="{{ URL::asset('css/styles.css') }}" type="text/css"/>

    <script src="{{ URL::asset('js/jquery.min.js') }}" type="text/javascript"> </script>
    <script src="{{ URL::asset('js/moment.min.js') }}" type="text/javascript"> </script>
    <script src="{{ URL::asset('materialize/js/materialize.js') }}" type="text/javascript"> </script>
    <script src="{{ URL::asset('js/jquery.validate.min.js') }}" type="text/javascript"> </script>
    @yield('page-level-js')
    <script src="{{ URL::asset('js/scripts.js') }}" type="text/javascript"> </script>
    {{--<script src="{{ URL::asset('js/scripts-min.js') }}" type="text/javascript"> </script>--}}
    <script src="{{ URL::asset('js/pagination.js') }}" type="text/javascript" ></script>
    <script src="{{ URL::asset('js/appraisal.js') }}" type="text/javascript" ></script>

</head>
<body>

@yield('content')

<!-- BEGIN: Footer-->
<footer class="page-footer footer blue darken-4">
    <div class="footer-copyright">
        <div class="container"><span>&copy; {{date('Y')}} <a class="" href="#" target="_blank">PPDA</a> All rights reserved.</span></div>
    </div>
</footer>
<!-- END: Footer-->



</body>
</html>
 