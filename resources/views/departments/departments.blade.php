
@extends('layouts.master-admin')
@section('title')
    System Settings - Add Department
@endsection

@section('content')

    <div class="col s12 spacer"></div>

    <div class="row">

        <div class="col l4 s12">
            <div class="card-panel col l0 offset-l1 m12 s12">

                <div id="register" class="col s12">
                    <form class="col s12" method="post" action="{{route('create_department')}}">

                        <div class="form-container">
                            <h5 class=""><i class="material-icons right blue-text text-darken-4">add_circle_outline</i>Add Department</h5>

                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="department" name="department" type="text" class="validate" required value="{{old('department')}}">
                                    <label for="department">Department</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col s12">
                                    @if(count($errors->all()) > 0)
                                        <ul>
                                            @foreach($errors->all() as $error)
                                                <li class="invalid">{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="col s12 spacer"></div>
                                <div class="col s12 center">
                                    <button class="btn waves-effect waves-light camel-case blue darken-4" type="submit" name="action">Create Department</button>
                                </div>
                            </div>
                            {{csrf_field()}}
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col l8 m12 s12">
            <div class=" card-panel col m10 offset-m1 s12">

                <div id="register" class="col s12">

                    <div class="form-container">
                        <h5 class=""><i class="material-icons right blue-text text-darken-4">list</i>User Departments</h5>

                        <div class="row">
                            <table class="bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Department</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>

                                <tbody>

                                @if(isset($departments) && count($departments) > 0)
                                    @foreach($departments as $department)
                                        <tr>
                                            <td>{{ $loop->iteration }} </td>
                                            <td>{{$department->department}}</td>
                                            <td><a data-record-id="{{$department->id}}" class="green-text edit-button-department" href="#modal{{$department->id}}"><i class="material-icons center">edit</i></a></td>
                                            <td><a class="red-text" href="{{route('delete_department',$department->id)}}" onclick="showDeleteConfirmationModal(this, 'modal_del_{{$department->id}}'); return false;"><i class="material-icons center">delete_forever</i></a></td>

                                            {{--include department view modal --}}
                                            <div id="modal{{$department->id}}" class="modal ">
                                                <div class="modal-content">
                                                    @include('departments.department-form-edit')
                                                </div>
                                            </div>

                                            {{--include department delete modal --}}
                                            @include('departments.department-delete-modal')

                                        </tr>

                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4" class="center-align">No departments found</td>
                                    </tr>
                                @endif

                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>
            </div>
        </div>

    </div>

    @if(isset($message))

        <div id="modal_account_created" class="modal">
            <div class="modal-content">
                <h5>Operation Successful</h5>
                <p>{{$message}}</p>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">OK</a>
            </div>
        </div>

    @endif

@endsection
