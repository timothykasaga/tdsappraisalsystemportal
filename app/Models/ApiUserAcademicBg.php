<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 6/7/2019
 * Time: 18:04
 */


namespace app\Models;


class ApiUserAcademicBg {

    public $id;
    public $institution;
    public $yearOfStudy;
    public $award;
    public $username;

}