<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 6/1/2019
 * Time: 12:18
 */


namespace app\Models;


class ApiFormCompetenceAssessmentSummary {

    public $id;
    public $sectionEPercentageScore;
    public $sectionEWeighedScore;
    public $sectionDScore;
    public $sectionEScore;
    public $appraisalTotalScore;

}