<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/31/2019
 * Time: 20:13
 */


namespace app\Models;


class ApiFormCompetenceCategory {

    public $id;
    public $orgCode;
    public $employeeCategoryCode;
    public $competenceCategory;
    public $maxRating;
    public $appraisalCompetences;

}