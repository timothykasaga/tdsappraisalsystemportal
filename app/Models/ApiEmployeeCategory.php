<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/17/2019
 * Time: 00:05
 */


namespace app\Models;


class ApiEmployeeCategory {

    public $category = "";
    public $categoryCode = "";
    public $orgCode = "";
    public $createdBy = "";

}