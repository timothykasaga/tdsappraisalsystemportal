<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/30/2019
 * Time: 22:42
 */


namespace app\Models;


class ApiFormAppraiserComment {

    public $id;
    public $recommendation;
    public $comment;

}