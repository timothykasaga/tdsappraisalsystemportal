<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/30/2019
 * Time: 20:54
 */


namespace app\Models;


class ApiFormAdditionalAssignment {

    public $id;
    public $objectiveId;
    public $expectedOutput;
    public $actualPerformance;
    public $maximumRating;
    public $appraiseeRating;
    public $appraiserRating;
    public $agreedRating;

}