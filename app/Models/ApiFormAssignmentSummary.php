<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 6/1/2019
 * Time: 12:16
 */


namespace app\Models;


class ApiFormAssignmentSummary {

    public $id;
    public $sectionDPercentageScore;
    public $sectionDWeighedScore;
    public $appraiserComment;

}