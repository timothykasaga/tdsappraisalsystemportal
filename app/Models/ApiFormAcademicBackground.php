<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/30/2019
 * Time: 12:03
 */


namespace app\Models;


class ApiFormAcademicBackground {

    public $id;
    public $appraisalRef;
    public $school;
    public $year;
    public $award;

}
