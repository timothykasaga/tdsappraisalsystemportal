<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/20/2019
 * Time: 14:26
 */


namespace app\Models;


class ApiApplicationStat {

    public $countAppraisals  = "0";
    public $countAppraisalsNew  = "0";
    public $countUsers  = "0";
    public $countUsersNew  = "0";
    public $countRegionalOffices  = "0";
    public $countRegionalOfficesNew  = "0";
    public $countDepartments  = "0";
    public $countDepartmentsNew  = "0";

}