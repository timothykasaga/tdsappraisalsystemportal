<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 5/30/2019
 * Time: 23:22
 */


namespace app\Models;


class ApiFormStrengthAndWeakness {

    public $id;
    public $strengths;
    public $weaknesses;

}