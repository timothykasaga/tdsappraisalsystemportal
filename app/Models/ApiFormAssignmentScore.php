<?php
/**
 * Created by Leontymo Developers.
 * User: timothy kasaga
 * Date: 6/1/2019
 * Time: 12:16
 */


namespace app\Models;


class ApiFormAssignmentScore {

    public $id;
    public $totalMaximumRating;
    public $totalAppraiseeRating;
    public $totalAppraiserRating;
    public $totalAgreedRating;

}